var gsaApp = angular.module('gsaApp', ['ngSanitize'])
                .config(function($locationProvider) {
                  $locationProvider.html5Mode({
                    enabled: true,
                    requireBase: false
                    });
                });

gsaApp.controller('gsaCtrl', ['$scope','$http','$location','$sce', function($scope,$http,$location,$sce) {
    var searchObject = $location.search();
    console.log(searchObject);

    $scope.formData = {};

      $http({
        method  : 'GET',
        url     : '/searching',
        params    : { search: searchObject},
        // headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
      })
      .success(function(data) {
        var valores = data.GSP.RES.R;
        var start = 0;
        valores.forEach(function (value,i) {
          data.GSP.RES.R[i].S = data.GSP.RES.R[i].S.replace(/<br>/g,'');
        });

        var start = 0;
        // Snippets
        $scope.formRes = data.GSP.RES.R;
        // Resultado Global
        $scope.Datos = data.GSP;
        for (var i = 0; i < data.GSP.PARAM.length; i++) {
          if(data.GSP.PARAM[i].name == "start") {
            console.log(data.GSP.PARAM[i].name + " --> " + data.GSP.PARAM[i].value);
              if (data.GSP.PARAM[i].value != "undefined") start = parseInt(data.GSP.PARAM[i].value);
          }
        }
        // Numero maximo de resultados
        maxResults = parseInt(data.GSP.RES.M);
        $scope.maxResults = maxResults; // Numero maximo de documentos
        // Paginación
        if ((start + 10) <= maxResults ) {
          start += 10;
          $scope.lastPage = 1; // Es ultima pagina
          console.log("entro");
        } else {
          $scope.lastPage = 0; // No es ultima pagina
          start = 11;
        }

        $scope.start = start; // Documento de inicio
        $scope.pagina = (start/10) ; // pagina actual
        $scope.paginaDe = (maxResults/10); // Numero de paginas
        // Categorias
        $scope.categorias = data.GSP.RES.PARM.PMT[0].PV // Array de categorias
        // Busqueda
        var busqueda = encodeURIComponent(data.GSP.Q);
        var rawURL= busqueda.split("inmeta");
        // Es primera pagina?
        if (start > 10) {
          $scope.firstPage = 1;
          if(start == 11) {
            var prevURL = "results?search=" + busqueda +"&start=0";
          } else{
            var prevURL = "results?search=" + busqueda +"&start="+(start-20);
          }
        } else {
          $scope.firstPage = 0;
          var prevURL = "results?search=" + busqueda;
        }
        var nextURL = "results?search=" + busqueda +"&start="+start;
        $scope.prevURL=prevURL; //URL a pagina previa
        $scope.nextURL=nextURL; //URL a pagina siguiente
        $scope.catURL = "results?search=" + busqueda + "+inmeta:category%3D"; //URL con Filtro
        $scope.baseURL = "results?search=" + rawURL[0]; // URL sin filtro

        if (!data.success) {
          // if not successful, bind errors to error variables
          $scope.errorName = data;
        } else {
          // if successful, bind success message to message
          $scope.message = data;
        }
      });
    //};
//  }
}]);

// Sugerencias en la búsqueda

$(function() {
  $( "#searchNavBar" ).autocomplete({
  source: function( request, response ) {

  var q = jQuery("#searchNavBar").val();

      $.ajax({
    dataType : "jsonp",
    jsonp : "callback",
          type : "Get",
          url: "http://10.38.5.75/suggest?q="+q+"&max=10&site=default_collection&client=json_frontend&format=rich",
          success: function(data) {
      var results = [];

      data.results.forEach(function(result) {
        if(result.type == "suggest" ){
          results.push(result.name);
        }
      });

      response(results);
    }
      });
    },
    minLength: 2
  });
});
