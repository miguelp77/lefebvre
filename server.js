var express = require("express");
var request = require("request")
var path = require('path')

var app = express();
app.use(express.static(__dirname + '/public'));

var router = express.Router();
var views = __dirname + '/views/';

router.use(function (req,res,next) {
  console.log("/" + req.method);
  next();
});

router.get("/",function(req,res){
  res.sendFile(views + "index.html");
});

router.get("/about",function(req,res){
  res.sendFile(views + "about.html");
});

router.get("/results",function(req,res){
  var q = req.query.search;
  res.sendFile(views + "results.html");
});


router.get("/contact",function(req,res){
  res.sendFile(views + "contact.html");
});

app.use("/",router);

app.use("*",function(req,res){
  res.sendFile(views + "404.html");
});

router.get('/searching', function(req, res, next) {
    var parametro = req.query.search;
    var parseado = JSON.parse(parametro);
    var startValue = 0;
    if (parseado.start != "undefined") {
      var startValue = parseado.start;
    }
    // console.log("sss "+parseado.start);
    var trimado = parseado.search.trim();
    // console.log(trimado);
    // console.log(startValue);
    var parEncode = encodeURIComponent(trimado);
    var url = "http://10.38.5.75/search?q="+parEncode+"&site=default_collection&btnG=Google+Search&access=p&client=json_frontend&output=xml_no_dtd&proxystylesheet=json_frontend&sort=date%3AD%3AL%3Ad1&wc=200&wc_mc=1&oe=UTF-8&ie=UTF-8&ud=1&exclude_apps=1&filter=0&start="+startValue;
    request({
      url: url,
      json: true
    }, function (error, response, body) {
      if (!error && response.statusCode === 200) {
        res.send(body);
      } else {
        console.log(error);
      }
    });
});


app.listen(3000,function(){
  console.log("Live at Port 3000");
});
